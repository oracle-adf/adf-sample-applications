package view.samples;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.adf.view.rich.render.ClientEvent;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class SampleBean {
    private RichInputText inputText2;

    public SampleBean() {
    }

    public void setInputText2(RichInputText inputText2) {
        this.inputText2 = inputText2;
    }

    public RichInputText getInputText2() {
        return inputText2;
    }

    private void setFocus(UIComponent comp) {
        if (comp != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            StringBuilder script = new StringBuilder();
            script.append("var comp = AdfPage.PAGE.findComponent('").append(comp.getClientId(context)).append("'); ").append("comp.focus();");
            ExtendedRenderKitService erks =
                Service.getService(context.getRenderKit(),
                                   ExtendedRenderKitService.class);
            erks.addScript(context, script.toString());
        }
    }

    public void popupOpenServerEvent(ClientEvent event) {
        System.out.println("Popup opened");
        if (this.inputText2 != null) {
            this.setFocus(this.inputText2);
        }
    }
}
