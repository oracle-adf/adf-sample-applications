package com.subbanasolutions.view.bean;

import oracle.adf.model.RegionBinding;
import oracle.adf.model.RegionContext;
import oracle.adf.model.RegionController;
import oracle.adf.view.rich.context.AdfFacesContext;

public class SampleViewBean implements RegionController {
    public SampleViewBean() {
        super();
    }

    @Override
    public boolean refreshRegion(RegionContext regionContext) {
        int refreshFlag = regionContext.getRefreshFlag();
        if (refreshFlag == RegionBinding.PREPARE_MODEL) {
            // Invoke Initialization method
            System.out.println(":::: Initialization Method Invoked");
            this.initializeMethod();
        }
        regionContext.getRegionBinding().refresh(refreshFlag);
        return false;
    }

    @Override
    public boolean validateRegion(RegionContext regionContext) {
        regionContext.getRegionBinding().validate();
        return false;
    }

    @Override
    public boolean isRegionViewable(RegionContext regionContext) {
        return false;
    }

    @Override
    public String getName() {
        return null;
    }

    public void initializeMethod() {
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        adfFacesContext.getPageFlowScope().put("InitializationText",
                                               "Initialization Text set during on load of page fragement");
    }
}
